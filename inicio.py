import sys
import math
from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, QMessageBox


class Inicio(QMainWindow):
		def __init__(self):
				super(Inicio, self).__init__()
				uic.loadUi("inicio.ui", self)
				self.setWindowIcon(QIcon("imagenes/icono.ico"))
				self.showMaximized()

				self.pushButtonInicio.clicked.connect(self.abrirSegundaPantalla)

		def abrirSegundaPantalla(self):
				self.segundaPantalla = SegundaPantalla()
				self.segundaPantalla.show()
				self.close()


class SegundaPantalla(QMainWindow):
		def __init__(self):
				super(SegundaPantalla, self).__init__()
				uic.loadUi("segundaPantalla.ui", self)
				self.setWindowIcon(QIcon("imagenes/icono.ico"))
				self.showMaximized()

				self.pushButtonResolverEj1.clicked.connect(self.resolverEjercicio1)
				self.pushButtonResolverEj2.clicked.connect(self.resolverEjercicio2)
				self.pushButtonResolverEj3.clicked.connect(self.resolverEjercicio3)
				self.pushButtonResolverEj4.clicked.connect(self.resolverEjercicio4)

		def contieneValoresNegativosEj1(self):
				mensajeError = ""
				if(float(self.a1v.text()) < 0 or float(self.a1sen.text()) < 0 or float(self.a1h.text()) < 0):
						mensajeError += "Valores negativos en A1.\n"
				if(float(self.a2o.text()) < 0 or float(self.a2360.text()) < 0 or float(self.a2r2.text()) < 0):
						mensajeError += "Valores negativos en A2.\n"
				if(float(self.a3r1.text()) < 0 or float(self.a3cos.text()) < 0 or float(self.a3r2.text()) < 0 or float(self.a3sen.text()) < 0):
						mensajeError += "Valores negativos en A3.\n"
				if(float(self.vl.text()) < 0):
						mensajeError += "Valores negativos en V.\n"
				if(float(self.wgama.text()) < 0):
						mensajeError += "Valores negativos en W.\n"
				if(float(self.apr1.text()) < 0 or float(self.apr2.text()) < 0 or float(self.apcos.text()) < 0 or float(self.apl.text()) < 0):
						mensajeError += "Valores negativos en Ap.\n"
				if(float(self.hch.text()) < 0 or float(self.hcr1.text()) < 0 or float(self.hccos1.text()) < 0 or float(self.hcr2.text()) < 0 or float(self.hcr3.text()) < 0 or float(self.hccos2.text()) < 0):
						mensajeError += "Valores negativos en hc.\n"
				if(float(self.fhgama.text()) < 0):
						mensajeError += "Valores negativos en FH.\n"
				return mensajeError

		def resolverEjercicio1(self):
				resultado = self.contieneValoresNegativosEj1()
				if(resultado != ""):
						ventanaAviso = QMessageBox()
						ventanaAviso.setWindowTitle("Error")
						ventanaAviso.setText(resultado)
						ventanaAviso.exec_()
				else:
						a1res = float(self.a1v.text(
						)) * math.sin(math.radians(float(self.a1sen.text()))) * float(self.a1h.text())
						self.a1res.setText(str(round(a1res, 2)))

						a2res = (float(self.a2o.text()) / float(self.a2360.text())) * \
								(math.pow(float(self.a2r2.text()), 2) * math.pi)
						self.a2res.setText(str(round(a2res, 2)))

						a3res = (float(self.a3r1.text()) * math.cos(math.radians(float(self.a3cos.text())))) * \
								(float(self.a3r2.text()) *
								 math.sin(math.radians(float(self.a3sen.text())))) / 2
						self.a3res.setText(str(round(a3res, 2)))

						ares = float(a1res) + float(a2res) + float(a3res)
						self.ares.setText(str(round(ares, 2)))

						self.va.setText(self.ares.text())
						vres = float(self.va.text()) * float(self.vl.text())
						self.vres.setText(str(round(vres, 2)))

						self.wv.setText(self.vres.text())
						wres = float(self.wgama.text()) * float(self.wv.text())
						self.wres.setText(str(round(wres, 2)))

						self.fvres.setText(self.wres.text())

						apres = (float(self.apr1.text()) - (float(self.apr2.text()) *
																								math.cos(math.radians(float(self.apcos.text()))))) * float(self.apl.text())
						self.apres.setText(str(round(apres, 2)))

						hcres = float(self.hch.text()) + float(self.hcr1.text()) * \
								math.cos(math.radians(float(self.hccos1.text()))) + (float(self.hcr2.text()) -
																																		 float(self.hcr3.text()) * math.cos(math.radians(float(self.hccos1.text())))) / 2
						self.hcres.setText(str(round(hcres, 2)))

						self.fhap.setText(self.apres.text())
						self.fhhc.setText(self.hcres.text())
						fhres = float(self.fhgama.text()) * \
								float(self.fhhc.text()) * float(self.fhap.text())
						self.fhres.setText(str(round(fhres, 2)))

						self.frfv.setText(self.fvres.text())
						self.frfh.setText(self.fhres.text())
						frres = math.sqrt(math.pow(float(self.frfv.text()),
															2) + math.pow(float(self.frfh.text()), 2))
						self.frres.setText(str(round(frres, 2)))

						self.thetafv.setText(self.fvres.text())
						self.thetafh.setText(self.fhres.text())
						thetares = math.degrees(
								math.atan(float(self.thetafv.text()) / float(self.thetafh.text())))
						self.thetares.setText(str(round(thetares, 2)))

		def contieneValoresNegativosEj2(self):
			mensajeError = ""
			if(float(self.y1a.text()) < 0 or float(self.y1sen.text()) < 0):
				mensajeError += "Valores negativos en y1.\n"
			if(float(self.sr.text()) < 0):
				mensajeError += "Valores negativos en S.\n"
			if(float(self.hch2.text()) < 0):
				mensajeError += "Valores negativos en hc.\n"
			if(float(self.a1r.text()) < 0 or float(self.a1cos.text()) < 0):
				mensajeError += "Valores negativos en A.\n"
			if(float(self.a1h2.text()) < 0):
				mensajeError += "Valores negativos en A1.\n"
			if(float(self.a3r.text()) < 0 or float(self.a3grad.text()) < 0):
				mensajeError += "Valores negativos en A3.\n"
			if(float(self.gamasg.text()) < 0 or float(self.gamab.text()) < 0):
				mensajeError += "Valores negativos en gama.\n"
			return mensajeError

		def resolverEjercicio2(self):
			resultado = self.contieneValoresNegativosEj2()
			if(resultado != ""):
					ventanaAviso = QMessageBox()
					ventanaAviso.setWindowTitle("Error")
					ventanaAviso.setText(resultado)
					ventanaAviso.exec_()
			else:
				y1res = float(self.y1a.text()) * \
						math.sin(math.radians(float(self.y1sen.text())))
				self.y1res.setText(str(round(y1res, 2)))

				self.sy1.setText(str(y1res))
				sres = float(self.sr.text()) - float(self.sy1.text())
				self.sres.setText(str(round(sres, 2)))

				self.hcy1.setText(self.y1res.text())
				self.hcs.setText(self.sres.text())
				hcres2 = float(self.hch2.text()) + \
						float(self.hcy1.text()) + (float(self.hcs.text()) / 2)
				self.hcres2.setText(str(round(hcres2, 2)))

				a1res2 = float(self.a1r.text()) * \
						math.cos(math.radians(float(self.a1cos.text())))
				self.a1res2.setText(str(round(a1res2, 2)))

				self.a1a1.setText(self.a1res2.text())
				a1res3 = float(self.a1a1.text()) * float(self.a1h2.text())
				self.a1res3.setText(str(round(a1res3, 2)))

				self.a2h.setText(self.a1res2.text())
				self.a2b.setText(self.y1res.text())
				a2res2 = (float(self.a2h.text()) * float(self.a2b.text())) / 2
				self.a2res2.setText(str(round(a2res2, 2)))

				a3res2 = math.pi * float(math.pow(float(self.a3r.text()), 2)) * float(self.a3grad.text()) / 360
				self.a3res2.setText(str(round(a3res2, 2)))

				at = float(self.a1res3.text()) + float(self.a2res2.text()) + float(self.a3res2.text())
				self.at.setText(str(round(at, 2)))

				gamares = float(self.gamasg.text()) * float(self.gamab.text())
				self.gamares.setText(str(float(round(gamares, 2))))

				self.fhgama2.setText(self.gamares.text())
				self.fhhc2.setText(self.hcres2.text())
				self.fhs.setText(self.sres.text())
				fhres2 = float(self.fhgama2.text()) * float(self.fhhc2.text()) * float(self.fhs.text()) * 4
				self.fhres2.setText(str(round(fhres2, 2)))

				self.fvgama.setText(self.gamares.text())
				self.fvat.setText(self.at.text())
				fvres2 = float(self.fvgama.text()) * float(self.fvat.text()) * 4
				self.fvres2.setText(str(round(fvres2, 2)))

				self.frfv2.setText(self.fvres2.text())
				self.frat.setText(self.fhres2.text())
				frres2 = math.sqrt( math.pow(float(self.frfv2.text()), 2) + math.pow(float(self.frat.text()), 2))
				self.frres2.setText(str(round(frres2, 2)))

		def contieneValoresNegativosEj3(self):
			mensajeError = ""
			if(float(self.a1r3.text()) < 0 or float(self.a1h3.text()) < 0):
				mensajeError += "Valores negativos en A1.\n"
			if(float(self.a2r.text()) < 0):
				mensajeError += "Valores negativos en A2.\n"
			if(float(self.cuanprof.text()) < 0):
				mensajeError += "Valores negativos en ∀.\n"
			if(float(self.fvgama3.text()) < 0):
				mensajeError += "Valores negativos en Fv.\n"
			if(float(self.fhgama3.text()) < 0 or float(self.fhhc3.text()) < 0 or float(self.fha.text()) < 0):
				mensajeError += "Valores negativos en Fh.\n"

			return mensajeError

		def resolverEjercicio3(self):
			resultado = self.contieneValoresNegativosEj3()
			if(resultado != ""):
					ventanaAviso = QMessageBox()
					ventanaAviso.setWindowTitle("Error")
					ventanaAviso.setText(resultado)
					ventanaAviso.exec_()
			else:
				a1res4 = float(self.a1r3.text()) * float(self.a1h3.text())
				self.a1res4.setText(str(round(a1res4, 2)))

				a2res3 = math.pi * math.pow(float(self.a2r.text()), 2) / 4
				self.a2res3.setText(str(round(a2res3, 2)))

				self.a1.setText(self.a1res4.text())
				self.a2.setText(self.a2res3.text())
				ares3 = float(self.a1res4.text()) + float(self.a2res3.text())
				self.ares3.setText(str(round(ares3, 2)))

				self.cuana.setText(self.ares3.text())
				cuanres = float(self.cuana.text()) * float(self.cuanprof.text())
				self.cuanres.setText(str(round(cuanres, 2)))

				self.fvcuan.setText(self.cuanres.text())
				fvres3 = (float(self.fvgama3.text()) * float(self.fvcuan.text())) / 1000
				self.fvres3.setText(str(round(fvres3, 2)))

				fhres3 = (float(self.fhgama3.text()) * float(self.fhhc3.text()) * float(self.fha.text())) / 1000
				self.fhres3.setText(str(round(fhres3, 2)))

				self.frfh3.setText(self.fvres3.text())
				self.frfv3.setText(self.fhres3.text())
				frres3 = math.sqrt( math.pow(float(self.frfh3.text()), 2) + math.pow(float(self.frfv3.text()), 2) )
				self.frres3.setText(str(round(frres3, 2)))


				self.thetafv3.setText(self.fvres3.text())
				self.thetafh3.setText(self.fhres3.text())
				thetares3 = math.degrees(
									math.atan(float(self.thetafv3.text()) / float(self.thetafh3.text())))
				self.thetares3.setText(str(round(thetares3, 2)))

				self.xa1.setText(self.a1res4.text())
				self.xa2.setText(self.a2res3.text())
				self.xa.setText(self.ares3.text())
				xres = ( float(self.xa1.text()) * 0.375 + float(self.xa2.text()) * 0.318 ) / float(self.xa.text())
				self.xres.setText(str(round(xres, 2)))

		def contieneValoresNegativosEj4(self):
			mensajeError = ""
			if(float(self.a14.text()) < 0 or float(self.a24.text()) < 0):
				mensajeError += "Valores negativos en A.\n"
			if(float(self.frgama.text()) < 0 or float(self.frhc.text()) < 0):
				mensajeError += "Valores negativos en Fr.\n"
			if(float(self.icb.text()) < 0 or float(self.ich.text()) < 0):
				mensajeError += "Valores negativos en Ic.\n"
			if(float(self.lplc.text()) < 0):
				mensajeError += "Valores negativos en Lp - Lc.\n"
			if(float(self.fhlc_2.text()) < 0 or float(self.fhlc.text()) < 0):
				mensajeError += "Valores negativos en Fh.\n"
			if(float(self.fs1.text()) < 0 or float(self.fsh.text()) < 0):
				mensajeError += "Valores negativos en Fs.\n"
			return mensajeError

		def resolverEjercicio4(self):
			resultado = self.contieneValoresNegativosEj4()
			if(resultado != ""):
					ventanaAviso = QMessageBox()
					ventanaAviso.setWindowTitle("Error")
					ventanaAviso.setText(resultado)
					ventanaAviso.exec_()
			else:
				ares4 = float(self.a14.text()) * float(self.a24.text())
				self.ares4.setText(str(round(ares4, 2)))

				self.fra.setText(self.ares4.text())
				frres4 = float(self.frgama.text()) * float(self.frhc.text()) * float(self.fra.text())
				self.frres4.setText(str(round(frres4, 2)))

				icres = (float(self.icb.text()) * math.pow(float(self.ich.text()), 3)) / 12
				self.icres.setText(str(round(icres, 2)))

				self.lpic.setText(self.icres.text())
				self.lpa.setText(self.ares4.text())
				lpres = float(self.lpic.text()) / ( float(self.lplc.text()) * float(self.lpa.text()) )
				self.lpres.setText(str(round(lpres, 2)))

				self.fhfr.setText(self.frres4.text())
				fhres4 = ( float(self.fhfr.text()) * float(self.fhlc_2.text()) ) / float(self.fhlc.text())
				self.fhres4.setText(str(round(fhres4, 2)))

				self.fsfr.setText(self.frres4.text())
				fsres = ( float(self.fsfr.text()) * float(self.fs1.text()) ) / float(self.fsh.text())
				self.fsres.setText(str(round(fsres, 2)))


if __name__ == "__main__":
		app = QApplication(sys.argv)
		principal = Inicio()
		principal.show()
		sys.exit(app.exec_())
